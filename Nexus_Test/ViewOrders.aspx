﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewOrders.aspx.cs" Inherits="Nexus_Test._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div >
        <asp:GridView ID="OrderGridView" 
                        HeaderStyle-BackColor="#3AC0F2" 
                        HeaderStyle-ForeColor="White"
                        runat="server"
                        AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField 
                            DataField="Order_Id" 
                            HeaderText="Order Id" />
                <asp:BoundField
                            DataField="Customer_Name"
                            HeaderText="Customer"/>
                <asp:BoundField
                            DataField="Name"
                            HeaderText="Product"/>
                <asp:BoundField
                            DataField="Price"
                            HeaderText="Price"/>
                <asp:BoundField
                            DataField="Quantity"
                            HeaderText="Quantity"/>
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

<HeaderStyle BackColor="#990000" ForeColor="White" Font-Bold="True"></HeaderStyle>
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </div>
</asp:Content>
