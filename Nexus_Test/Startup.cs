﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nexus_Test.Startup))]
namespace Nexus_Test
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
