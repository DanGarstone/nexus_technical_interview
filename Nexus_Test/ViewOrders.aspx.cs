﻿using Nexus_Test.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

namespace Nexus_Test
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try {
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(
                                        new DataColumn[5] {
                                                    new DataColumn("Order_Id", typeof(Guid)),
                                                    new DataColumn("Customer_Name", typeof(string)),
                                                    new DataColumn("Name", typeof(string)),
                                                    new DataColumn("Price", typeof(decimal)),
                                                    new DataColumn("Quantity", typeof(int))});

                    SqlConnection con = new SqlConnection();
                    con.ConnectionString = ConfigurationManager.AppSettings["NexusConnectionString"];
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "SELECT O.ORDER_ID, O.CUSTOMER_NAME, P.NAME, P.PRICE, OD.QUANTITY " +
                                        "FROM \"ORDER\" O " +
                                            "INNER JOIN " +
                                                "ORDER_DETAIL OD " +
                                            "ON O.ORDER_ID = OD.ORDER_ID " +
                                            "INNER JOIN " +
                                                "PRODUCT P " +
                                            "ON OD.PRODUCT_ID = P.PRODUCT_ID " +
                                            "ORDER BY O.CUSTOMER_NAME";
                    cmd.Connection = con;
                    SqlDataAdapter rd = new SqlDataAdapter(cmd);
                    rd.Fill(dt);
                    con.Close();
                    con.Dispose();
                    if (dt != null && OrderGridView != null)
                    {
                        OrderGridView.DataSource = dt;
                        OrderGridView.DataBind();
                    }
                }
                catch(SqlException e2)
                {
                    throw new Exception("An error occured when retrieving the orders", e);
                }
            }
        }
    }
}