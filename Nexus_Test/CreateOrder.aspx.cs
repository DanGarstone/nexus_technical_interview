﻿using Nexus_Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nexus_Test
{
    public partial class CreateOrder : Page
    {
        public List<Product> products = new List<Product>();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable prods = new DataTable();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["NexusConnectionString"];
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT PRODUCT_ID, NAME FROM PRODUCT";
            cmd.Connection = con;
            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            rd.Fill(prods);

            con.Close();
            con.Dispose();

            if(ProductsDropDown != null && prods != null)
            {
                ProductsDropDown.DataSource = prods;
                ProductsDropDown.DataBind();
            }
        }

        public void submitBtn_Click(Object sender, EventArgs e)
        {
            var orderId = Guid.NewGuid();
            var orderDetailId = Guid.NewGuid();
            int value;
            if(QuantityTB == null || !int.TryParse(QuantityTB.ToString(), out value))
            {

            }

            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["NexusConnectionString"];
            con.Open();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandText = "INSERT INTO \"ORDER\" VALUES ('" + orderId + "', '" + CustomerNameTB.Text + "')";
                cmd.Connection = con;
                if (!(cmd.ExecuteNonQuery() > 0))
                {
                    throw new Exception("No records were affected when creating an order.");
                }
            } 
            catch(Exception ex)
            {
                throw new Exception("An error occured while creating the new order with id " + orderId + " while using SQL command: " + cmd.CommandText, ex);
            }

            try
            {
                cmd.CommandText = "INSERT INTO ORDER_DETAIL VALUES('"+ orderDetailId+"', '"+orderId+"', '"+ProductsDropDown.SelectedValue.ToString()+"', '"+QuantityTB.Text+"')";
                cmd.Connection = con;
                if (!(cmd.ExecuteNonQuery() > 0))
                {
                    throw new Exception("No records were affected when creating the order detail");
                }
            }   
            catch(SqlException e2)
            {
                throw new Exception("An error occured while creating a new order details with id " + orderDetailId + " while using SQL command: " + cmd.CommandText, e2.InnerException);
            }
            finally
            {
                con.Close();
                con.Dispose();
                Response.Redirect("ViewOrders.aspx");
            }
        }
    }
}