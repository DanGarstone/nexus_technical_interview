﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nexus_Test.Models
{
    public class OrderDetail
    {
        public Guid OrderDetailId { get; set; }
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}