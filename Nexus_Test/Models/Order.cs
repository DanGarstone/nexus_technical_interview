﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nexus_Test.Models
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public string CustomerName { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
}