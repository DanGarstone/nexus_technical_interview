﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nexus_Test.Models
{
    public class V_Orders
    {
        public Guid OrderId { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}