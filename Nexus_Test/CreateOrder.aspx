﻿<%@ Page Title="Create Order" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateOrder.aspx.cs" Inherits="Nexus_Test.CreateOrder" %>


    <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <div>
            <asp:Label width="150px" runat="server" Text="Customer Name:"></asp:Label>
            <asp:TextBox runat="server" ID="CustomerNameTB"></asp:TextBox>
            <br />
            <asp:Label width="150px" runat="server" Text="Product:"></asp:Label>
            <asp:DropDownList ID="ProductsDropDown" runat="server" AppendDataBoundItems="True" DataTextField="NAME" DataValueField="PRODUCT_ID"></asp:DropDownList>
            <br />
            <asp:Label width="150px" runat="server" Text="Quantity"></asp:Label>
            <asp:TextBox runat="server" ID="QuantityTB"></asp:TextBox>
            <br />
            <asp:Button runat="server" ID="SubmitBtn" OnClick="submitBtn_Click" Enabled="true" Text="Save"/>
        </div>
    </asp:Content>
